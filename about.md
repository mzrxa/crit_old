---
layout: page
title: عن الموقغ
permalink: /about/
---
<br>

وَلَا تَهِنُوا وَلَا تَحْزَنُوا 

<br>

<div style="margin: 100px 50px 50px 50px; float:center">
    <a rel="me" href="https://pleroma.envs.net/mzrxa"><img border="0" alt="pleroma" src="{{ site.baseurl}}/images/twatt.svg" height="30"></a>

<br>

<div style="float:center">
    <a rel="me" href="https://mastodon.online/@mzrxa"><img border="0" alt="masto" src="{{ site.baseurl}}/images/masto.svg" height="30"></a>
</div>