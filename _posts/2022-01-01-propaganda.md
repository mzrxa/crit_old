---
layout: post
title: سقوف التنك في ادبيات طق الحنك
image: "/images/taz22.png"
dir: ltr
alt: platform
---


rtlrt

Start

The United Nation Office for Project Services (UNOPS) launched the Sustainable Integrated Municipal Action Project (SIMA) in light of the post-2019 events in Lebanon. The project aimed at helping Lebanese Municipalities overcome the challenges of the looming crisis. The SIMA-SN (Street Network) sub-project proposed an upheaval of existing pedestrian infrastructure in Beirut and Bourj Hammoud (BH) in strategically picked locations.

Team

KREDO, AA and BC worked in tandem to overcome the challenges faced by the municipalities of Beirut and Bourj Hammoud. 

Plan

Phase 1 literature review, data collection and site inspection provided the team with a bridged reading of theoretical frameworks and minute actually-existing situations on the ground. 

Phase 2 planned a brief for further action in accordance

AJ

Strategically the tactical interventions (low resource - high impact) should be at the center of the presentation. What might appear as mundane retrofitting would be of major importance if smartly located or implemented.

HP:
Highly vulnerable geographies prioritization (impoverished suburbs and challenging urbanities).
Theoretical:
Rent gap > Low rent > Urban Limit > Pop Density > Pedestrians ++
Vulnerability map
Simple Measures - High Impact
Providing pedestrian infrastructure to counter private car ownership hegemony Beirut.
Pedestrians on the forefront of urban policy.
Integrating pedestrian and mass transit networks.

TC:
Coordinated actions and community involvement
Quantitative reviews for selecting areas of intervention 

VE:
Matrix approach to funding distribution
Emphasis on nodal intervention and high volume areas
O
l
this is a test we can manage
this is another test well we can't manage

&nbsp;

this is yet another test **with bold action**

&nbsp;

### and this is a header
